FROM mcr.microsoft.com/dotnet/framework/sdk:4.8-20240109-windowsservercore-ltsc2019
ENV chocolateyUseWindowsCompression=false
ENV chocolateyVersion=1.4.0
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop';"]
RUN Set-ExecutionPolicy Bypass -Scope Process -Force; \
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; \
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
RUN choco feature enable --name allowGlobalConfirmation 
RUN choco install visualstudio2017professional --confirm --ignore-pending-reboot
RUN choco install visualstudio2017-workload-python --version=1.0.2
RUN choco install visualstudio2017-workload-nativedesktop --version=1.2.3
RUN choco install hxd --version=2.5.0.0
RUN choco install python --version=3.6.8
RUN choco install notepadplusplus --version=8.1.3
RUN choco install git --version=2.41.0
RUN choco install winscp --version=6.1.1
RUN choco install putty --version=0.78
RUN choco install 7zip.install --version=23.1.0
RUN Invoke-WebRequest -OutFile "Firefox-Installer.exe" -Uri https://download.mozilla.org/?product=firefox-stub
RUN Start-Process -FilePath .\Firefox-Installer.exe -ArgumentList "/S"
RUN choco install jdk8 --version=8.0.211

